import presentacion from '/_presentacion.html';
import educacion from '/_educacion.html';
import experiencia from '/_experiencia.html';
import formacion from '/_formacion.html';
import contacto from '/_contacto.html';

let fragments = {presentacion, educacion, experiencia, formacion, contacto};

const TAB_BAR_ITEM = "TabBar__item";
const SELECTED = `${TAB_BAR_ITEM}--selected`;

document.querySelectorAll(`.${TAB_BAR_ITEM}[data-fragment]`).forEach(item => {
  item.addEventListener('click', event => {
    // Marcamos el nuevo seleccionado
    let selected = item.parentNode.querySelector(`.${SELECTED}`);
    selected?.classList.remove(SELECTED);
    item.classList.add(SELECTED);

    // Cargamos el contenido
    let target = document.querySelector(item.parentNode.dataset.target);
    target.innerHTML = fragments[item.dataset.fragment];
  });
});
document.querySelector(`.${TAB_BAR_ITEM}[data-fragment="presentacion"]`).click();
